#include <stdio.h>
#include <string.h>
#include <math.h>
#include "ephem.h"

int main()
{

  double LP0,LP1,D0,D1,MP0,MP1,F0,F1;
  double Me0,Me1,Ve0,Ve1,Te0,Te1,Ma0,Ma1,Ju0,Ju1,Sa0,Sa1;
  double ang,arg0,arg1;
  char str[120], *path=DATADIR;
  int i,m0,m1,m2,m3,m4,m5,m6,m7,m8,m9;
  FILE *fp;

  LP0 = 218.31665436;
  LP1 = 481267.88134240;

  D0 = 297.85020420;
  D1 = 445267.11151675;

  MP0 = 134.96341138;
  MP1 = 477198.86763133;

  F0 = 93.27209932;
  F1 = 483202.01752731;

  Me0 = 252.250906;
  Me1 = 149472.674636;

  Ve0 = 181.979801;
  Ve1 = 58517.815676;

  Te0 = 100.466450;
  Te1 = 35999.372854;

  Ma0 = 355.433275;
  Ma1 = 19140.299331;

  Ju0 = 34.351484;
  Ju1 = 3034.905675;

  Sa0 = 50.077471;
  Sa1 = 1222.113794;

  i=0;
  fp = myopen(path,"mksvp.dat","r");
  fgets(str,120,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%lf",
       &m1,&m2,&m3,&m4,&m5,&m6,&m7,&m8,&m9,&ang);
    arg0 = (m6*LP0 + m7*D0 + m8*MP0 + m9*F0);
    arg0 += (m1*Ve0 + m2*Te0 + m3*Ma0 + m4*Ju0 + m5*Sa0) + ang;
    arg0 -= 360.0*floor(arg0/360.0);
    arg1 = (m6*LP1 + m7*D1 + m8*MP1 + m9*F1);
    arg1 += (m1*Ve1 + m2*Te1 + m3*Ma1 + m4*Ju1 + m5*Sa1);
    printf("%2d  %8.4lf\t%12.4lf\n",++i,arg0,arg1);
    fgets(str,120,fp);
  } 
  fclose(fp);

  return 0;
}
