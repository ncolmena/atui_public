#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ephem.h"

/* myopen opens a file.  It's just like fopen, except:			*/
/* -  accepts separate path and file arguments (simply concatenated)	*/
/* -  dies with message to stderr if the open fails			*/
/*									*/
/* usage: myopen(path,file,mode)					*/

FILE *myopen(char *path, char *file, char *mode)
{
	char filename[256];
	FILE *fp;

	strcpy(filename,path);
	strcat(filename,file);
	fp = fopen(filename,mode);
	if (fp == NULL)
	{
		fprintf(stderr,"could not open file %s\n", filename);
		exit(1);
	}
	return fp;
}
