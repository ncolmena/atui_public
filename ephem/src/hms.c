#include <math.h>
#include "ephem.h"

void hms(double decimaldeg, int *hh, int *mm, double *ss)
{
  double dec;
  int sign;

  if (decimaldeg < 0.0) {
    sign = -1;
  } else {
    sign = 1;
  }

  dec = decimaldeg;
  dec *= sign;

  *hh = floor(dec);
  *mm = floor((dec - *hh)*60.0);
  *ss = ((dec - *hh)*60.0 - *mm)*60.0;

   *hh *= sign;
}
