#include <stdio.h>
#include <math.h>
#include "ephem.h"

void mm3x3(double a[], double b[], double c[]);

int main()
{

  double a[9],b[9],c[3],d[9];
  double *aptr,*bptr,*cptr;
  int i;

  c[0] = 1.0;
  c[1] = 2.0;
  c[2] = 3.0;
  printf("c = %lf,%lf,%lf\n",c[0],c[1],c[2]);

  cptr = c;
  *cptr++ = 4.0;
  *cptr++ = 5.0;
  *cptr++ = 6.0;
  printf("c = %lf,%lf,%lf\n",c[0],c[1],c[2]);

  aptr = a;
  for (i = 0 ; i < 9 ; i++) {
    *aptr++ = i;
  }
  bptr = b;
  for (i=0; i<9; i++) { *bptr++ = 0.0; }
  b[0] = 1.0;
  b[4] = 2.0;
  b[8] = 3.0;

  printf("\na[0,*] = %lf,%lf,%lf\n",a[0],a[1],a[2]);
  printf("a[1,*] = %lf,%lf,%lf\n",a[3],a[4],a[5]);
  printf("a[2,*] = %lf,%lf,%lf\n",a[6],a[7],a[8]);

  printf("\nb[0,*] = %lf,%lf,%lf\n",b[0],b[1],b[2]);
  printf("b[1,*] = %lf,%lf,%lf\n",b[3],b[4],b[5]);
  printf("b[2,*] = %lf,%lf,%lf\n",b[6],b[7],b[8]);

  mm3x3(b,a,d);

  printf("\nd[0,*] = %lf,%lf,%lf\n",d[0],d[1],d[2]);
  printf("d[1,*] = %lf,%lf,%lf\n",d[3],d[4],d[5]);
  printf("d[2,*] = %lf,%lf,%lf\n",d[6],d[7],d[8]);

  return 1;
}

void mm3x3(double a[], double b[], double c[])
{

  double *aptr,*bptr,*cptr;
  int i,j;

  cptr = c;

/*  *cptr++ = a[0]*b[0] + a[1]*b[3] + a[2]*b[6];	*/
/*  *cptr++ = a[0]*b[1] + a[1]*b[4] + a[2]*b[7];	*/
/*  *cptr++ = a[0]*b[2] + a[1]*b[5] + a[2]*b[8];	*/
/*							*/
/*  *cptr++ = a[3]*b[0] + a[4]*b[3] + a[5]*b[6];	*/
/*  *cptr++ = a[3]*b[1] + a[4]*b[4] + a[5]*b[7];	*/
/*  *cptr++ = a[3]*b[2] + a[4]*b[5] + a[5]*b[8];	*/
/*							*/
/*  *cptr++ = a[6]*b[0] + a[7]*b[3] + a[8]*b[6];	*/
/*  *cptr++ = a[6]*b[1] + a[7]*b[4] + a[8]*b[7];	*/
/*  *cptr++ = a[6]*b[2] + a[7]*b[5] + a[8]*b[8];	*/

  for (i=0; i<3; i++){
    for (j=0; j<3; j++){
      *cptr++ = a[3*i]*b[j] + a[3*i+1]*b[j+3] + a[3*i+2]*b[j+6];
/*      printf("i = %d, j = %d, sum = %lf\n",i,j,*(cptr-1));	*/
    }
  }

}
