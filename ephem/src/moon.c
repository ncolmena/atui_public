#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "ephem.h"

int main(int argc, char *argv[])
{

  double second;
  double day,jd1,zero,doy,arg,dt,dut1,jde,utc,tdt,mjd,mjdtai,st,ss,ss2,tmp;
  double bt,LP,D,M,MP,F,c0,c1,E,OM,P,L0;
  double sv,svp,svpp,svppp,su,sup,supp,suppp,sr,srp,srpp,srppp;
  double vdot,udot,radot,decdot,denom,longitude,latitude,range;
  double sunlong,moonphase,sunra,sundec,sunha,sunaz,sunelev,sunel,refsun;
  double W,tana,anga,incl,lambda,beta,liblon,liblat,psi,eps,obliq,eclip;
  double k1,k2,rho,tau,sigma,pliblon,pliblat,v,x,y,omega,p;
  double alpha,delta,topodelt,topora,hourang,ha_geo,azimuth,elev;
  double stationlat,stationelev,temp,press,ref;
  double mrot[9],crot[9],totrot[9],rmoon[3];
  double slong,slat,l,b,cl,sl,cb,sb,cp,sp,ca,sa,cd,sd;
  double rafeat,decfeat,rangefeat,ra,dec,rng,sign;
  double earth_omega,lat,decrad,ha,rmag,vmult,azmag,elmag,vaz,vel;
  double dv[3],zen[3],rvect[3],rhat[3],vr[3],vt[3],azvect[3],elvect[3];
  double v_earth[3],v_moon[3],azhat[3],elhat[3];
  double fudge; /* yum */
  int i,month,jan,intday,year,hour,minute,hh,mm,hh2,mm2;
  int m1,m2,m3,m4;
  long int ampl,mjdin;
  char str[80],feature[40],*f;
  char s1[10],s2[10],s3[10],s4[10],s5[10],s6[10],s7[10];
  char *path=DATADIR;
  FILE *fp;
//  FILE *dataout;

  struct tm *tp;
  time_t timenow;

  #ifdef __MACOS__
    #include <console.h>
    argc = ccommand(&argv);
  #endif

  f = feature;
  *f++ = 'C'; *f++ = 'E'; *f++ = 'N'; *f++ = 'T'; *f++ = 'E'; *f++ = 'R';
  *f++ = '\0';

  if (argc < 7) {		/* supply time as computer clock time now */
    timenow = time(NULL);
    tp = gmtime(&timenow);
    month = (*tp).tm_mon+1;
    intday = (*tp).tm_mday;
    year = (*tp).tm_year + 1900;
    hour = (*tp).tm_hour;
    minute = (*tp).tm_min;
    second = (double) (*tp).tm_sec;
  }
  if (argc == 2) { sscanf(argv[1],"%s",feature); }
  if (argc == 7){ 		/* read in time from command line	*/
    sscanf(argv[1],"%d",&month);
    sscanf(argv[2],"%d",&intday);
    sscanf(argv[3],"%d",&year);
    sscanf(argv[4],"%d",&hour);
    sscanf(argv[5],"%d",&minute);
    sscanf(argv[6],"%lf",&second);
  }
  if (argc == 8){ 		/* read in feature & time from command line */
    sscanf(argv[1],"%s",feature);
    sscanf(argv[2],"%d",&month);
    sscanf(argv[3],"%d",&intday);
    sscanf(argv[4],"%d",&year);
    sscanf(argv[5],"%d",&hour);
    sscanf(argv[6],"%d",&minute);
    sscanf(argv[7],"%lf",&second);

  }

  if (islower(feature[0]) != 0) {
    i = 0;
    while (feature[i] != '\0') {
      feature[i] = toupper(feature[i]);
      i++;
    }
  }

/* Compute Julian day: INPUT TIME IS IN TDT, NOT UT			*/

  day = intday + (hour + (minute + second/60.0)/60.0)/24.0;
  jde = julianday(month,day,year);
  mjd = jde - 2400000.5;
  tdt = 24.0*(mjd - floor(mjd));
  utc = tdt - (TDT_TAI + TAI_UTC)/3600.0;
  utc -= 24.0*floor(utc/24.0);

/* Compute JD of Jan 0 of this year for use as day of year (doy)	*/

  jan = 1;
  zero = 0.0;
  doy = jde - julianday(jan,zero,year);

/* Compute TDT - UT = DT and also convert to TAI in MJD seconds		*/

  dt = dyntime(jde);

  if (mjd > 52058.5 && mjd < 52423.5) {
    fp = myopen(path,"dut1.dat","r");
    fgets(str,80,fp);
    while (strstr(str,"END") == NULL) {
      sscanf(str,"%s%s%s%s%s%s%s",s1,s2,s3,s4,s5,s6,s7);
      sscanf(s4,"%ld",&mjdin);
      sscanf(s7,"%lf",&tmp);
      if (fabs(mjd - (double) mjdin) < 0.5) {
        dut1 = tmp;
      }
      fgets(str,80,fp);
    }
    fclose(fp);
    dt = TDT_TAI + TAI_UTC - dut1;
  }
  printf("DT = %lf\n",dt);

  jd1 = jde - dt/86400.0;
  mjdtai = (jde - TDT_TAI/86400.0 - 2400000.5)*86400.0;

/* This is the T (big-T) argument: julian centuries from 2000.0		*/

  bt = (jde - 2451545.0)/36525.0;

/*  Arguments from Meeus, reportedly from Chapront (1998) French publ.	*/
/*  LP = 218.3164477 + (481267.88123421 - (0.0015786 - (1.0/538841.0 -	*/
/*       (bt/65194000.0))*bt)*bt)*bt;					*/
/*  D = 297.8501921 + (445267.1114034 - (0.0018819 - (1.0/545868.0 -	*/
/*       (bt/113065000.0))*bt)*bt)*bt;					*/
/*  M = 357.5291092 + (35999.0502929 - (0.0001536 - (bt/24490000.0))*bt)*bt; */
/*									*/
/*  MP = 134.9633964 + (477198.8675055 + (0.0087414 + (1.0/69699.0 -	*/
/*       (bt/14712000.0))*bt)*bt)*bt;					*/
/*  F  = 93.2720950 + (483202.0175233 - (0.0036539 + (1.0/3526000.0 -	*/
/*       (bt/863310000.0))*bt)*bt)*bt;					*/
  OM = 125.0445479 - (1934.1362891 - (0.0020754 + (1.0/467441.0 -
       (bt/60616000.0))*bt)*bt)*bt;
  P = 83.3532465 + (4069.0137287 - (0.0103200 + (1.0/80053.0 -
       (bt/18999000.0))*bt)*bt)*bt;

/*  Arguments from Chapronts' Lunar Tables... (1991), Willmann-Bell	*/
/*  LP -> L; M -> l-prime; MP -> l; 					*/
  LP = 218.31665436 + (481267.88134240 - (0.00013268 - (1.0/538793.0 -
       (bt/65189000.0))*bt)*bt)*bt;
  D = 297.85020420 + (445267.11151675 - (0.0001630 - (1.0/545841.0 -
      (bt/113122000.0))*bt)*bt)*bt;
  M = 357.52910918 + (35999.05029094 - (0.0001536 - (bt/24390240.0))*bt)*bt;

  MP = 134.96341138 + (477198.86763133 + (0.0089970 + (1.0/69696.0 -
       (bt/14712000.0))*bt)*bt)*bt;
  F = 93.27209932 + (483202.01752731 - (0.0034029 + (1.0/3521000.0 -
       (bt/862070000.0))*bt)*bt)*bt;
  L0 = 280.46646 + (36000.76983 + 0.0003032*bt)*bt;

/* Correction term for changing eccentricity (not used by Chapronts)	*/

  E = 1.0 - 0.002516*bt - 0.0000074*bt*bt;
  
  LP -= 360.0*floor(LP/360.0);
  D  -= 360.0*floor(D/360.0);
  M  -= 360.0*floor(M/360.0);
  MP -= 360.0*floor(MP/360.0);
  F  -= 360.0*floor(F/360.0);
  OM -= 360.0*floor(OM/360.0);
  P  -= 360.0*floor(P/360.0);
  L0 -= 360.0*floor(L0/360.0);

  printf("%02u/%02u/%4u at %02u:%02u:%07.4f\n",
         month,intday,year,hour,minute,second);
/*  printf("day= %lf\n",day);	*/
  printf("JD = %lf; JDE= %lf; MJDTAI = %lf\n",jd1,jde,mjdtai);
  printf("T  = %14.12lf\n",bt);
/*  printf("LP = %lf\n",LP);	*/
/*  printf("D  = %lf\n",D);	*/
/*  printf("M  = %lf\n",M);	*/
/*  printf("MP = %lf\n",MP);	*/
/*  printf("F  = %lf\n",F);	*/
/*  printf("OM = %lf\n",OM);	*/
/*  printf("P  = %lf\n",P);	*/
/* printf("E  = %lf\n",E);	*/

  sv = 0.0;
  svp = 0.0;
  svpp = 0.0;
  svppp = 0.0;
  su = 0.0;
  sup = 0.0;
  supp = 0.0;
  suppp = 0.0;
  sr = 0.0;
  srp = 0.0;
  srpp = 0.0;
  srppp = 0.0;
  vdot = 481267.88;
  udot = 0.0;

/* read terms from files and add series for long, lat, range, vdot, udot  */

  
  fp = myopen(path,"sv.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    sv += (double) ampl * 0.00000001 * sin(arg);
    fgets(str,80,fp);
  } 
  fclose(fp);

  
  fp = myopen(path,"svp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t\t%lf\t\t%lf",&ampl,&c0,&c1);
    arg = DTR * fmod(c0 + c1*bt, 360.0);
    svp += (double) ampl * 0.00001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"svpp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t\t%lf\t\t%lf",&ampl,&c0,&c1);
    arg = DTR * fmod(c0 + c1*bt, 360.0);
    svpp += (double) ampl * 0.00001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"svppp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    svppp += (double) ampl * 0.01 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"su.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    su += (double) ampl * 0.00000001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"sup.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t\t%lf\t\t%lf",&ampl,&c0,&c1);
    arg = DTR * fmod(c0 + c1*bt, 360.0);
    sup += (double) ampl * 0.00001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"supp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    supp += (double) ampl * 0.00001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"suppp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    suppp += (double) ampl * 0.01 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"sr.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    sr += (double) ampl * 0.0001 * cos(arg);
    fgets(str,80,fp);
  } 
  fclose(fp);

  
  fp = myopen(path,"srp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t\t%lf\t\t%lf",&ampl,&c0,&c1);
    arg = DTR * fmod(c0 + c1*bt, 360.0);
    srp += (double) ampl * 0.0001 * cos(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"srpp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    srpp += (double) ampl * 0.0001 * cos(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"srppp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    srppp += (double) ampl * 0.1 * cos(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"vdot.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    vdot += (double) ampl * cos(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  
  fp = myopen(path,"udot.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    udot += (double) ampl * cos(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

/*  printf("sum of s_v terms is: %lf\n",sv);		*/
/*  printf("sum of s_v' terms is: %lf\n",svp);		*/
/*  printf("sum of s_v'' terms is: %lf\n",svpp);	*/
/*  printf("sum of s_v''' terms is: %lf\n",svppp);	*/
/*  printf("sum of s_u terms is: %lf\n",su);		*/
/*  printf("sum of s_u' terms is: %lf\n",sup);		*/
/*  printf("sum of s_u'' terms is: %lf\n",supp);	*/
/*  printf("sum of s_u''' terms is: %lf\n",suppp);	*/
/*  printf("sum of s_r terms is: %lf\n",sr);		*/
/*  printf("sum of s_r' terms is: %lf\n",srp);		*/
/*  printf("sum of s_r'' terms is: %lf\n",srpp);	*/
/*  printf("sum of s_r''' terms is: %lf\n",srppp);	*/
/*  printf("Vdot = %lf; Udot = %lf\n",vdot,udot);	*/

/*  printf("sum of long. terms is: %lf\n",suml);	*/
/*  printf("sum of lat.  terms is: %lf\n",sumb);	*/
/*  printf("sum of rad.  terms is: %lf\n",sumr);	*/

  longitude = LP + sv + 0.001*(svp + svpp*bt + 0.0001*svppp*bt*bt);
  latitude = su + 0.001*(sup + supp*bt + 0.0001*suppp*bt*bt);;
  range = 385000.57 + sr + srp + srpp*bt + 0.0001*srppp*bt*bt;

// figure out sun's longitude (geocentric, mean equinox of date)

  sunlong = L0 + (1.914602 - (0.004817 + 0.000014*bt)*bt)*sin(DTR*M);
  sunlong += (0.019993 - 0.000101*bt)*sin(2.0*DTR*M);
  sunlong += 0.000289*sin(3.0*DTR*M);
  moonphase = longitude - sunlong;
  moonphase -= 360.0*floor(moonphase/360.0);
  sunlong *= DTR;
  
  printf("Lunar phase angle: %lf\n",moonphase);

/*  printf("mean longitude, latitude is %lf, %lf\n",longitude,latitude);  */
/*  printf("range is %lf km\n",range);	*/

  longitude -= 0.00019524 + 0.00001059*sin(DTR*(MP+90.0));
  latitude -= 0.00001754*sin(DTR*(F+90.0));
  range += 0.0708*cos(DTR*(MP+90.0));

  printf("\nApparent longitude, latitude, range: %lf, %lf, %lf\n",
          longitude,latitude,range);

  nutation(jde,&psi,&eps,&obliq);
  eclip = DTR * (obliq + eps/3600.0);

/*  printf("Nutation is: (%lf,%lf), obliquity is: %lf\n",psi,eps,obliq);  */

/* Compute Lunar optical librations			*/

  incl = 1.54242 * DTR;
  lambda = (longitude  + psi/3600.0)* DTR;
  beta = latitude * DTR;
  W = lambda - OM * DTR;
  tana = (sin(W)*cos(beta)*cos(incl)-sin(beta)*sin(incl))/(cos(W)*cos(beta));
  liblon = atan(tana)*RTD - F;
  liblon -= 360.0*floor(liblon/360.0) + 180.0;
  if (liblon >  20.0) liblon -= 180.0;
  if (liblon < -20.0) liblon += 180.0;
  anga = (liblon + F)*DTR;
  liblat = -asin(sin(W)*cos(beta)*sin(incl) + sin(beta)*cos(incl))*RTD;

  printf("\nOptical libration in long., lat. is: (%lf,%lf)\n",liblon,liblat);

/* Compute Lunar physical librations			*/

  k1 = 119.75 + 131.849*bt;
  k2 = 72.56  +  20.186*bt;
  k1 -= 360.0*floor(k1/360.0);
  k2 -= 360.0*floor(k2/360.0);

  MP *= DTR;
  D  *= DTR;
  F  *= DTR;
  M  *= DTR;

  rho = -0.02752*cos(MP) - 0.02245*sin(F) + 0.00684*cos(MP-2*F);
  rho += -0.00293*cos(2*F) - 0.00085*cos(2*(F-D));
  rho += -0.00054*cos(MP-2*D) - 0.00020*sin(MP+F);
  rho += -0.00020*cos(MP+2*F) - 0.00020*cos(MP-F) + 0.00014*cos(MP+2*F-2*D);

  sigma = -0.02816*sin(MP) + 0.02244*cos(F) - 0.00682*sin(MP-2*F);
  sigma += -0.00279*sin(2*F) - 0.00083*sin(2*(F-D)) + 0.00069*sin(MP-2*D);
  sigma += 0.00040*cos(MP+F) - 0.00025*sin(2*MP) - 0.00023*sin(MP+2*F);
  sigma += 0.00020*cos(MP-F) + 0.00019*sin(MP-F) + 0.00013*sin(MP+2*(F-D));
  sigma -= 0.00010*cos(MP-3*F);

  tau = 0.02520*E*sin(M) + 0.00473*sin(2*(MP-F)) - 0.00467*sin(MP);
  tau += 0.00396*sin(k1*DTR) + 0.00276*sin(2*(MP-D)) + 0.00196*sin(OM*DTR);
  tau += -0.00183*cos(MP-F) + 0.00115*sin(MP-2*D) - 0.00096*sin(MP-D);
  tau += 0.00046*sin(2*(F-D)) - 0.00039*sin(MP-F) - 0.00032*sin(MP-M-D);
  tau += 0.00027*sin(2*MP - M - 2*D);
  tau += 0.00023*sin(k2*DTR) - 0.00014*sin(2*D) + 0.00014*cos(2*(MP-F));
  tau += -0.00012*sin(MP-2*F) - 0.00012*sin(2*MP) + 0.00011*sin(2*(MP-M-D));

/*  printf("rho = %lf; sigma = %lf; tau = %lf\n",rho,sigma,tau);	*/
/*  printf("A = %lf\n",anga*RTD);	*/

  pliblon = -tau + (rho*cos(anga) + sigma*sin(anga))*tan(liblat*DTR);
  pliblat = sigma*cos(anga) - rho*sin(anga);

/* Add physical libration to optical libration to get total	*/
  liblon += pliblon;
  liblat += pliblat;

  printf("Moon's physical librations are: (%lf,%lf)\n",pliblon,pliblat);
  printf("Total libration is: (%lf,%lf)\n",liblon,liblat);

/* Compute the local apparent siderial time		*/

  st = siderial(jd1) + APOLONG;
  st += psi*cos((obliq + eps/3600.0)*DTR)/3600.0;
  st -= 360.0*floor(st/360.0);
  tmp = st/15.0;
  hms(tmp,&hh,&mm,&ss);
/*  printf("\nApparent Siderial time is: %12.8lf = %02u:%02u:%07.4lf\n",  */
/*           st,hh,mm,ss);	*/

/* transform ecliptic to equatorial coordinates		*/

  ecliptoeq(jd1,lambda,beta,&alpha,&delta);
  ecliptoeq(jd1,sunlong,0.0,&sunra,&sundec);

/* Compute time derivatives of RA & dec in deg/s	*/

  vdot /= (86400.0 * 36525.0);
  udot /= (86400.0 * 36525.0);

  decdot = udot*(cos(beta)*cos(eclip)-sin(beta)*sin(eclip)*sin(lambda));
  decdot += vdot*cos(beta)*sin(eclip)*cos(lambda);
  decdot /= cos(delta * DTR);
  radot = vdot*(cos(eclip) - sin(lambda)*tan(beta)*sin(eclip));
  radot -= udot*sin(eclip)*cos(lambda)/(cos(beta)*cos(beta));
  denom = cos(lambda)*cos(lambda);
  denom += pow(sin(lambda)*cos(eclip) - tan(beta)*sin(eclip),2.0);
  radot /= denom;

/* Compute position angle of moon's rotation axis	*/

  v = (OM + psi/3600.0 + sigma/sin(incl))*DTR;
  x = sin(incl + rho*DTR)*sin(v);
  y = sin(incl + rho*DTR)*cos(v)*cos(eclip) - cos(incl + rho*DTR)*sin(eclip);
  omega = atan2(x,y);
  p = asin(sqrt(x*x+y*y)*cos(alpha*DTR - omega) / cos(DTR*liblat));
  
/*  printf("v=%lf, p=%lf, w=%lf\n",v*RTD,p*RTD,omega*RTD);	*/

/* Print out equatorial coordinates				*/

  tmp = alpha / 15.0;
  hms(tmp,&hh,&mm,&ss);
  hms(delta,&hh2,&mm2,&ss2);
  printf("\nApparent RA,dec: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",
          alpha,delta,hh,mm,ss,hh2,mm2,ss2);
  printf("RAdot, decdot = %11.9lf, %11.9lf\n",radot,decdot);

/* Compute position to lunar feature				*/

  l = liblon * DTR;
  b = liblat * DTR;
  cl = cos(l);  sl = sin(l);
  cb = cos(b);  sb = sin(b);
  cp = cos(p);  sp = sin(p);

  mrot[0] = cb*cl;            mrot[1] = cb*sl;           mrot[2] = sb;
  mrot[3] = sp*sb*cl-cp*sl;   mrot[4] = sp*sb*sl+cp*cl;  mrot[5] = -sp*cb;
  mrot[6] = -cp*sb*cl-sp*sl;  mrot[7] = sp*cl-cp*sb*sl;  mrot[8] = cp*cb;

  ca = cos(alpha * DTR);  sa = sin(alpha * DTR);
  cd = cos(delta * DTR);  sd = sin(delta * DTR);

  crot[0] = -ca*cd;  crot[1] = sa;   crot[2] = -ca*sd;
  crot[3] = -sa*cd;  crot[4] = -ca;  crot[5] = -sa*sd;
  crot[6] = -sd;     crot[7] = 0.0;  crot[8] = cd;

  mm3x3(crot,mrot,totrot);

  rmoon[0] = range*ca*cd;  rmoon[1] = range*sa*cd;  rmoon[2] = range*sd;

/* Figure out which lunar feature to go for			*/

  
  fp = myopen(path,"features.dat","r");
  fgets(str,80,fp);
  while (strstr(str,feature) == NULL){
    fgets(str,80,fp);
    if (strstr(str,"END") != NULL){
      printf("Failed to find %s in features.dat file\n",feature);
      return -1;
    }
  }
  fclose(fp);

  sscanf(str,"%s%lf\t\t%lf",s1,&slong,&slat);
  if (slong == 0.000 && slat == 0.000){
    slong = liblon;
    slat = liblat;
  }

/* For N,S poles, guard against possible singularity (problem?)	*/
  if (fabs(fabs(slat) - 90.0) < 0.001){
    sign = slat / fabs(slat);
    slat = sign * 89.9999;
  }
/* For E,W limbs, correct for libration in longitude		*/
  if (fabs(fabs(slong) - 90.0) < 0.001){
    slong += liblon;
  }

  printf("\nFeature [%s] at selenographic: (%lf, %lf)\n",feature,slong,slat);

  moonfeat(slong,slat,totrot,rmoon,&decfeat,&rafeat,&rangefeat);

/* From here on, set RA, dec, range equal to feature coordinates	*/

  ra = rafeat;
  dec = decfeat;
  rng = rangefeat;

/* Accomodate request for SELENOCENTER apparent coordinates 		*/

  if (strstr(feature,"SELENOCENTER") != NULL){
    ra = alpha;
    dec = delta;
    rng = range;
  }

/* Print out equatorial coordinates of lunar feature */

  tmp = ra/ 15.0;
  hms(tmp,&hh,&mm,&ss);
  hms(dec,&hh2,&mm2,&ss2);
  printf("Feature at: (%lf, %lf); Range: %10.3lf km = %10.8lf A.U.\n", 
          ra,dec,rng,rng/149597900.0);

/* Convert to topocentric coordinates at APO location		*/

  stationlat = APOLAT;
  stationelev = APOELEV;
  topo(st,ra,dec,rng,stationlat,stationelev,&topodelt,&topora);

  tmp = topora / 15.0;
  hms(tmp,&hh,&mm,&ss);
  hms(topodelt,&hh2,&mm2,&ss2);
  printf("\nTopocentric RA,dec: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",
          topora,topodelt,hh,mm,ss,hh2,mm2,ss2);

/* Convert topocentric equatorial into azimuth and elevation	*/

  hourang = st - topora;
  ha_geo = st - ra;
  azel(hourang,topodelt,&azimuth,&elev);
  sunha = st - sunra;
  azel(sunha,sundec,&sunaz,&sunelev);
  printf("Hour angle = %f; geocentric = %f\n",hourang,ha_geo);

/* Correct for atmospheric refraction				*/

  temp = 283.0;
  press = 717.0;
  ref = refraction(elev,temp,press);
  refsun = refraction(sunelev,temp,press);
  sunel = sunelev + refsun;

  printf("Sun azimuth, elevation = (%f,%f)\n",sunaz,sunel);

  tmp = elev + ref;
  hms(azimuth,&hh,&mm,&ss);
  hms(tmp,&hh2,&mm2,&ss2);
  printf("Azimuth, elevation: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",
          azimuth,tmp,hh,mm,ss,hh2,mm2,ss2);
  hms(tmp,&hh,&mm,&ss);
  if (fabs(ref) > 0.0) {
    printf("Refracted by %lf arcsec\n",
            ref*3600.0);
  }
/*  printf("%lf  %lf\n",doy,alpha); */

//  dataout = fopen("tracks.out","a");

// compute velocity offset

  earth_omega = 2.0*PI/(86400.0 - 236.0);
  lat = GEOCLAT*DTR;
  decrad = dec*DTR;
  ha = ha_geo*DTR;

  v_earth[0] = -earth_omega*R_APO*cos(lat);
  v_earth[1] = 0.0;
  v_earth[2] = 0.0;

//  printf("v_earth = < %f , %f , %f >\n",v_earth[0],v_earth[1],v_earth[2]);

  v_moon[0] = -rangefeat*(cos(ha)*cos(decrad)*radot*DTR 
              + sin(ha)*sin(decrad)*decdot*DTR);
  v_moon[1] = rangefeat*(sin(ha)*cos(decrad)*radot*DTR 
              - cos(ha)*sin(decrad)*decdot*DTR);
  v_moon[2] = rangefeat*cos(decrad)*decdot*DTR;

//  printf("v_moon = < %f , %f , %f >\n",v_moon[0],v_moon[1],v_moon[2]);

  dv[0] = v_moon[0] - v_earth[0];
  dv[1] = v_moon[1] - v_earth[1];
  dv[2] = v_moon[2] - v_earth[2];

//  printf("dv = < %f , %f , %f >\n",dv[0],dv[1],dv[2]);

  zen[0] = 0.0;
  zen[1] = cos(lat);
  zen[2] = sin(lat);

  rvect[0] = rangefeat*sin(ha)*cos(decrad);
  rvect[1] = rangefeat*cos(ha)*cos(decrad) - R_APO*cos(lat);
  rvect[2] = rangefeat*sin(decrad) - R_APO*sin(lat);

//  printf("rvect = < %f , %f , %f >\n",rvect[0],rvect[1],rvect[2]);

  rmag = vabs3(rvect);
  rhat[0] = rvect[0] / rmag;
  rhat[1] = rvect[1] / rmag;
  rhat[2] = rvect[2] / rmag;

//  printf("rhat = < %f , %f , %f >\n",rhat[0],rhat[1],rhat[2]);

  vmult = vdot3(dv,rvect)/rmag;
  vr[0] = vmult*rhat[0];
  vr[1] = vmult*rhat[1];
  vr[2] = vmult*rhat[2];

//  printf("vr = < %f , %f , %f >\n",vr[0],vr[1],vr[2]);

  vt[0] = dv[0] - vr[0];
  vt[1] = dv[1] - vr[1];
  vt[2] = dv[2] - vr[2];

//  printf("vt = < %f , %f , %f >\n",vt[0],vt[1],vt[2]);

  vcross3(rhat,zen,azvect);
  vcross3(azvect,rhat,elvect);
  azmag = vabs3(azvect);
  elmag = vabs3(elvect);
  for (i=0; i<3; i++)
  {
    azhat[i] = azvect[i]/azmag;
    elhat[i] = elvect[i]/elmag;
  }

//  printf("azhat = < %f , %f , %f >\n",azhat[0],azhat[1],azhat[2]);
//  printf("elhat = < %f , %f , %f >\n",elhat[0],elhat[1],elhat[2]);

  vaz = vdot3(vt,azhat)*2.0*RTARC/C;
  vel = vdot3(vt,elhat)*2.0*RTARC/C;

  printf("v_offset_az =  %f ; v_ofset_el = %f (arcseconds)\n",vaz,vel);

  hms(utc,&hh,&mm,&ss);
  printf("UTC = %02u:%02u:%05.2lf\n",hh,mm,ss);
//  fprintf(dataout,"%s: UTC = %02u:%02u:%05.2lf\n",feature,hh,mm,ss);

  printf("tcc track %lf, %lf/distance=%10.8lf geo /keep=bore\n",ra,dec,rng/149597870.7);
//  fprintf(dataout,"track %lf, %lf/distance=%10.8lf geo /keep=bore\n",ra,dec,rng/149597870.7);
          
/*  fudge = 0.9856; */
  fudge = 1.00;   /* Russ has since fixed anomalous rate discrepency */
  printf("tcc offset arc 0.0, 0.0, %11.9lf, %11.9lf, %13.2lf /computed\n",
          radot*fudge*cos(delta * DTR),fudge*decdot,mjdtai);
//  fprintf(dataout,"offset arc 0.0, 0.0, %11.9lf, %11.9lf, %13.2lf /computed\n\n",
//          radot*fudge*cos(delta * DTR),fudge*decdot,mjdtai);

//  fclose(dataout);

  return 0;
}
