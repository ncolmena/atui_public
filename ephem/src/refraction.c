#include <stdio.h>
#include <math.h>
#include "ephem.h"

#define DTR	0.0174532925199

/* Takes elevation above horizon, in degrees, and computes the refaction, */
/* also in degrees, based on the temperature (kelvin) and pressure (mB)   */
/* passed in.                                                             */

double refraction(double elev, double temp, double press)
{
  double refrac,correc;

  if (elev < -1.0) return 0.0;

  if (temp < 200.0 || temp > 320.0) {
    temp = 273.15;
    press = 1013.25;
    printf("Assuming standard temperature and pressure for refraction\n");
  }

  refrac = 1.0/tan(DTR*(elev + 7.31/(elev + 4.4))) + 0.0013515;
  refrac -= 0.06*sin(DTR*(14.7*refrac + 13));

  correc = press/1013.25 * 283.0 / temp;

  return (refrac * correc)/60.0;

}
