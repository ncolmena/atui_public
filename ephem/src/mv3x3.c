#include "ephem.h"

void mv3x3(double a[], double b[], double c[])

/* Takes a 3x3 matrix pointer, a, stored in a 1-d array nine		*/
/* elements long (row major, such that elements 0,1,2 go across a	*/
/* row, and 0,3,6 go down a column), and a 3x1 vector pointer, b,	*/
/* and multiplies a*b = c.  						*/

{

  double *cptr;
  int i;

  cptr = c;

  for (i=0; i<3; i++){
    *cptr++ = a[3*i]*b[0] + a[3*i+1]*b[1] + a[3*i+2]*b[2];
  }

  /*  *cptr++ = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];	*/
  /*  *cptr++ = a[3]*b[0] + a[4]*b[1] + a[5]*b[2];	*/
  /*  *cptr++ = a[6]*b[0] + a[7]*b[1] + a[8]*b[2];	*/
  
}
