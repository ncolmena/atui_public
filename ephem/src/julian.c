#include <stdio.h>
#include <math.h>
#include "ephem.h"

double julianday(int month, double day, int year)
{

  int a,b,m,y,intjd;
  double jd;

  m = month;
  y = year;

/*  printf("Within julianday, M/D/Y is %02u/%f/%4u\n",m,day,y); */

  if (month < 3) {
    m += 12;
    y -= 1;
  }

  if (year > 1582 || (year == 1582 && month > 9 && day > 5.0)) {
    a = floor(y/100);
    b = 2 - a + floor(a/4);
  } else { b=0; }

  intjd = floor(365.25*(y+4716)) + floor(30.6001*(m+1)) + b;

  jd = (double) intjd + day - 1524.5;

  return jd;

}

