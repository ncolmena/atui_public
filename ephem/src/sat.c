#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "ephem.h"
#include "sgp4.h"

int main(int argc, char *argv[])
{
  double second,day,jdc,jd1,dt,dut1,jde,mjd,mjdtai,yday;
  double zeroth=0.0,epoch,tsince,ss,ss2;
  double decel,jerk,bstar,incl,ranode,peri,meananom,meanmot,ecc;
  double x3[3],v3[3],x,y,z,xdot,ydot,zdot,xpysq;
  double range,alpha,delta,radot,decdot;
  double psi,eps,obliq,st,tmp,stationlat,stationelev;
  double topora,topodelt,hourang,azimuth,elev,temp,press,ref;
  int i,month,intday,year,jan=1,hour,minute,yy,epochyear,hh,mm,hh2,mm2;
  int jerkexp,bstarexp;
  long int jerkint,bstarint,eccint,mjdin;

  FILE *fp;
  char *path=DATADIR;
  char str[80],sat[15];
  char sp[] = " ",lclag[]="lageos",uclag[]="LAGEOS";
  char s1[20],s2[20],s3[20],s4[20],s5[20],s6[20],s7[20],s8[20],s9[20];

  struct tm *tp;
  time_t timenow;

  #ifdef __MACOS__
    #include <console.h>
    argc = ccommand(&argv);
  #endif

  if (argc < 2) {
    printf("usage:  sat SATELLITE month day year hour minute second\n");
    strcpy(sat,"LAGEOS 1");
  }

  if (argc > 1) {
    sscanf(argv[1],"%s",sat);
/*    if (strstr(sat,uclag) == NULL && strstr(sat,lclag) == NULL) { */
/*      printf("Must specify LAGEOS1 or LAGEOS2\n"); */
/*      return -1; */
/*    } */
    if (islower(sat[0]) != 0) {
      i=0;
      while (sat[i] != '\0') {
        sat[i] = toupper(sat[i]);
        i++;
      }
    }
    if (strstr(sat,uclag) != NULL){
      strcat(uclag,sp);
      strcat(uclag,&sat[6]);
      strcpy(sat,uclag);
    }
  }

  if (argc < 8) {
    timenow = time(NULL);
    tp = gmtime(&timenow);
    month = (*tp).tm_mon+1;
    intday = (*tp).tm_mday;
    year = (*tp).tm_year + 1900;
    hour = (*tp).tm_hour;
    minute = (*tp).tm_min;
    second = (*tp).tm_sec;
  } else {
    sscanf(argv[2],"%d",&month);
    sscanf(argv[3],"%d",&intday);
    sscanf(argv[4],"%d",&year);
    sscanf(argv[5],"%d",&hour);
    sscanf(argv[6],"%d",&minute);
    sscanf(argv[7],"%lf",&second);
  }

  day = intday + (hour + (minute + second/60.0)/60.0)/24.0;
  jdc = julianday(month,day,year);
  mjd = jdc - 2400000.5;

  dt = dyntime(jdc);
  if (mjd > 52058.5 && mjd < 52423.5) {
    fp = myopen(path,"dut1.dat","r");
    fgets(str,80,fp);
    while (strstr(str,"END") == NULL) {
      sscanf(str,"%s%s%s%s%s%s%s",s1,s2,s3,s4,s5,s6,s7);
      sscanf(s4,"%ld",&mjdin);
      sscanf(s7,"%lf",&tmp);
      if (fabs(mjd - (double) mjdin) < 0.5) {
        dut1 = tmp;
      }
      fgets(str,80,fp);
    }
    fclose(fp);
    dt = TDT_TAI + TAI_UTC - dut1;
  }
  dut1 = TDT_TAI + TAI_UTC - dt;

  jde = jdc + dt/86400.0;
  mjdtai = (jdc + TAI_UTC/86400.0 - 2400000.5)*86400.0;
  jd1 = jdc - dut1/86400.0;

  printf("Calculating for %s on %02u/%02u/%4u at %02u:%02u:%07.4lf UTC\n",
          sat,month,intday,year,hour,minute,second);
  printf("JD = %.6lf; MJDTAI = %lf\n",jdc,mjdtai);

  fp = myopen(path,"tle.dat","r");
  fgets(str,80,fp);
  while (strstr(str,sat) == NULL){
    fgets(str,80,fp);
/*    if (strstr(str,EOF) != NULL){
      printf("Failed to find %s in two-line element file\n",sat);
      return -1;
    } */
  }
  fgets(str,80,fp);
  sscanf(str,"%s%s%s%s%s%s%s%s%s",s1,s2,s3,s4,s5,s6,s7,s8,s9);
  sscanf(s4,"%2d%lf",&yy,&yday);
  sscanf(s5,"%lf",&decel);
  sscanf(s6,"%5ld%2d",&jerkint,&jerkexp);
  sscanf(s7,"%5ld%2d",&bstarint,&bstarexp);
  jerk = (double) jerkint / 100000.0 * pow(10.0,jerkexp);
  bstar = (double) bstarint / 100000.0 * pow(10.0,bstarexp);

  fgets(str,80,fp);
  sscanf(str,"%s%s%s%s%s%s%s%s",s1,s2,s3,s4,s5,s6,s7,s8);
  sscanf(s3,"%lf",&incl);
  sscanf(s4,"%lf",&ranode);
  sscanf(s5,"%ld",&eccint);
  sscanf(s6,"%lf",&peri);
  sscanf(s7,"%lf",&meananom);
  sscanf(s8,"%lf%6c",&meanmot);
  ecc = (double) eccint / 10000000.0;

  fclose(fp);

/*  printf("%d %f %g %g\n",yy,yday,decel,jerk); */
/*  printf("%f %f %g %f %f %lf\n",incl,ranode,ecc,peri,meananom,meanmot); */

  if (yy < 65){
    epochyear = yy + 2000;
  } else {
    epochyear = yy + 1900;
  }
  epoch = julianday(jan,zeroth,epochyear);
  epoch += yday;

  printf("Epoch is %lf; offset is %lf days\n",epoch,jdc-epoch);

  tsince = (jdc - epoch) * XMNPDA;

  sgp4(incl,ranode,ecc,peri,meananom,meanmot,bstar,tsince,x3,v3);
  x = x3[0];
  y = x3[1];
  z = x3[2];
  xdot = v3[0];
  ydot = v3[1];
  zdot = v3[2];

  range = sqrt(x*x + y*y + z*z);
  alpha = atan2(y,x);
  alpha *= RTD;
  alpha -= 360.0*floor(alpha/360.0);
  delta = RTD * asin(z/range);

  xpysq = x*x + y*y;
  radot = (x*ydot - y*xdot)/xpysq;
  decdot = range*range*zdot - z*(x*xdot + y*ydot + z*zdot);
  decdot /= range*range*sqrt(xpysq);

  printf("\n RA = %f;  RAdot = %f\n",alpha,radot*RTD);
  printf("dec = %f; decdot = %f\n",delta,decdot*RTD);

  nutation(jde,&psi,&eps,&obliq);

  st = siderial(jd1) + APOLONG;
/*  printf("ST = %g, PSI = %g, EPS = %g, OBLIQ = %g\n",st,psi,eps,obliq); */
  st += psi*cos((obliq + eps/3600.0)*DTR)/3600.0;
  st -= 360.0*floor(st/360.0);
  tmp = st/15.0;
  hms(tmp,&hh,&mm,&ss);
  printf("\nApparent Siderial time is: %12.8lf = %02u:%02u:%07.4lf\n",
           st,hh,mm,ss);

  tmp = alpha / 15.0;
  hms(tmp,&hh,&mm,&ss);
  hms(delta,&hh2,&mm2,&ss2);
  printf("\nApparent RA,dec: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",
          alpha,delta,hh,mm,ss,hh2,mm2,ss2);
/*  printf("\nApparent geocentric RA (dec. hours), dec (deg.): (%lf, %lf)\n",*/
/*            alpha/15.0,delta);*/

  printf("Geocentric range = %lf km = %12.10lf A.U.\n",range, range/AUTOKM);

  stationlat = APOLAT;
  stationelev = APOELEV;
  topo(st,alpha,delta,range,stationlat,stationelev,&topodelt,&topora);

  tmp = topora / 15.0;
  hms(tmp,&hh,&mm,&ss);
  hms(topodelt,&hh2,&mm2,&ss2);
/*  printf("\nTopocentric RA,dec: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",	*/
/*          topora,topodelt,hh,mm,ss,hh2,mm2,ss2);	*/
  printf("\nTopocentric RA, dec: (%lf, %lf) => (%lf, %lf)\n",
            topora,topodelt,topora/15.0,topodelt);

  hourang = st - topora;
  azel(hourang,topodelt,&azimuth,&elev);

  temp = 283.0;
  press = 717.0;
  ref = refraction(elev,temp,press);
  tmp = elev + ref;
/*  azimuth += 180.0;*/
  if (azimuth > 360.0) azimuth -= 360.0;
  hms(azimuth,&hh,&mm,&ss);
  hms(tmp,&hh2,&mm2,&ss2);
  printf("Azimuth, elevation: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",
          azimuth,elev+ref,hh,mm,ss,hh2,mm2,ss2);

  if (fabs(ref) > 0.0) {
    printf("Elevation refracted by %lf arcsec\n",ref*3600.0);
  }

  printf("\nUTC = %02u:%02u:%09.6lf\n",hour,minute,second);
  printf("%f, %f\n",alpha,delta);
  printf("%f, %f\n",radot*RTD,decdot*RTD);
  printf("%f, %f\n",azimuth,elev+ref);

  return 0;

}

